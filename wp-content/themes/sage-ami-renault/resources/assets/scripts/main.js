// import external dependencies
import 'jquery';
import '@staaky/fresco';
import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/collapse';
import 'bootstrap/js/dist/tooltip';
import 'js-cookie';

// Import Vendors


// Partials
import './partials/acf-map';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import templateArchiveModel from './routes/templateArchiveModel';
import templateArchiveStock from './routes/templateArchiveStock';
import templateContact from './routes/templateContact';
import singleModel from './routes/singleModel';
import singleStock from './routes/singleStock';
import templateSplash from './routes/templateSplash';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,

  // Model overview
  templateArchiveModel,
  templateArchiveStock,
  // Contact page
  templateContact,
  // Model detail page
  singleModel,
  singleStock,
  // Splash Page
  templateSplash
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
