import objectFitImages from 'object-fit-images';
import Swiper from 'swiper/bundle';
import Sticky from 'sticky-js';
import SmoothScroll from 'smooth-scroll';

export default {
  init() {

    $(window).scroll(function () {
      if ($(window).scrollTop() > 750) {
        $('.main .quicklinks').not('.closed').addClass('active');
      } else {
        $('.main .quicklinks').removeClass('active');
      }
    });


    $('#custom-quote select:not([id^=fld_concession]), #custom-testride select:not([id^=fld_concession])').on('change', function () {
      let modal = $(this).closest('.modal');
      let title = $(this).children('option:selected').text();
      let postId = $(this).children('option:selected').val();

      modal.find('.form__product-name').text(title);
      modal.find('.form__product-image').css('opacity', 0).css('filter', 'blur(1rem) grayscale(1)')
      $.get('/wp-json/ami/v1/model/' + postId + '/thumbnail', function (thumbnail) {
        modal.find('.form__product-image').css('opacity', 1).css('filter', '')
        if (thumbnail) {
          modal.find('.form__product-image img').attr('src', thumbnail);
          modal.find('.form__product-image img').removeAttr('srcset');
        }
      })
    });

    $('.modal select[id^=fld_concession]').on('change', function (i, el) {
      const modal = $(this).closest('.modal')
      const concession_id = $(this).children('option:selected').val()

      $.get('/wp-json/ami/v1/concession/' + concession_id + '/phone', function (phone) {
        modal.find('.phone').text(phone)
        modal.find('.phone').attr('href', 'tel:' + phone)
      })
    })

    $('.js-hamburger-toggle').click(function (e) {
      e.preventDefault();
      $(this).siblings('.sub-menu').toggleClass('active');
      $(this).toggleClass('active');
    });

    $('.js-testride-form').click(function (e) {
      e.preventDefault();
      $('.modal').modal('hide');
      $('.modal-backdrop').remove();
      $('#custom-testride').modal('show');
    });

    $('.js-contact-form').click(function (e) {
      e.preventDefault();
      $('.modal').modal('hide');
      $('.modal-backdrop').remove();
      $('#contact').modal('show')
    });

    $('.js-quote-form').click(function (e) {
      e.preventDefault();
      $('.modal').modal('hide');
      $('.modal-backdrop').remove();
      $('#custom-quote').modal('show')
    });

    $('.quicklinks ').click(function (e) {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active').addClass('closed');
      } else {
        $(this).addClass('active');
      }
    })
    /**
     * Mobile navigation toggle
     */
    var $hamburger = $(".hamburger");
    $hamburger.on("click", function (e) {
      $hamburger.toggleClass("is-active");
      $('.hamburger-navigation').toggleClass('active');
    });


    /**
     * Model hover navigation
     */
    $('body').click(function (e) {
      if (!$(e.target).parents('.models-hover').length && !$(e.target).parent('.js-model-hover').length) {
        $('.models-hover').removeClass('active')
      }
    });

    // Escape closes the hover menu
    $(document).on('keyup', function (e) {
      if (e.key === "Escape") {
        $('.models-hover').removeClass('active');
        $('.header__search-form').removeClass('active');
      }
    });

    // If active, the model link closes the hover menu
    $('.js-model-hover').click(function (e) {
      e.preventDefault();

      if (!$(this).hasClass('active')) {
        $('.models-hover').addClass('active');
        $(this).addClass('active');
      } else {
        $('.models-hover').removeClass('active');
        $(this).removeClass('active');
      }
    });

    // Close key closes the menu
    $('.models-hover__nav-close').click(function (e) {
      e.preventDefault();
      $('.models-hover').removeClass('active')
    });

    // Clicking outside of the hover menu closes it
    $('.models-hover__nav-item').click(function (e) {
      e.preventDefault();
      let target = $(this).data('target');

      // Active class on button
      $('.models-hover__nav-item').removeClass('active');
      $(this).addClass('active');

      // Show correct category
      $('.models-hover__category').removeClass('active');
      $('.models-hover__category[data-category="' + target + '"]').addClass('active');
    });


    /**
     *  Header Search Input
     */
    $('.header__search').click(function (e) {
      e.preventDefault();
      $('.header__search-form').addClass('active');
    })

    $('.header__search-form-close').click(function (e) {
      e.preventDefault();
      $('.header__search-form').removeClass('active');
    })

    /**
     * Copy slider
     * @type {Swiper}
     */
    const sliderCopy = new Swiper('.slider__copy', {
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.slider__next',
        prevEl: '.slider__prev',
      },
      effect: 'fade',
      loop: true,
      speed: 500,
      fadeEffect: {
        crossFade: true
      },
      autoplay: {
        delay: 10000,
      }
    });

    // init Image Slider:
    const sliderImages = new Swiper('.slider__images', {
      effect: 'fade',
      loop: true,
      speed: 1000,
      fadeEffect: {
        crossFade: true
      },
    });

    // Connect sliders
    if (sliderCopy.controller != null && sliderImages.controller != null) {
      sliderCopy.controller.control = sliderImages;
      sliderImages.controller.control = sliderCopy;
    }

    /**
     * Browser friendly smooth scroll
     */
    const scroll = new SmoothScroll('a[href*="#"]');


    /**
     * Object fit polyfill for IE
     */
    objectFitImages();


    /**
     * Sticky navigation
     */
    const sticky = new Sticky('.sticky', {
      wrap: true,
      stickyClass: 'js-is-sticky',
    });

    /**
     * Fix Caldera form modal layout
     */
    $('.modal').each(function () {
      $(this).find('.final-step').appendTo($(this).find('.caldera-grid .breadcrumb'));
      $(this).find('.caldera-grid .breadcrumb').prependTo($(this).find('.form__steps'));
    });

    $('.breadcrumb a').click(function (e) {
      e.preventDefault();
    });


    let currentForm, currentFormId;
    $(document).on('cf.ajax.request', function (event, obj) {
      currentForm = obj.$form;
      currentFormId = obj.formIdAttr;
    }).on('cf.complete', function () {
      let $modal = currentForm.closest('.modal');
      $modal.find('li.active').addClass('complete');
      $modal.find('li').removeClass('active');
      $modal.find('.final-step').addClass('active');
    });

    // Activate tooltips
    $("[data-toggle='tooltip']").tooltip();

    // make titles special for dacia
    $('.section-title').each((i, el) => {
      let split = el.innerHTML.trim().split(' ')
      if (split.length <= 1) return;
      let half = Math.floor(split.length / 2)
      el.innerHTML = split.slice(0, half).join(' ') + ' <span>' + split.slice(half).join(' ') + '</span>'
    })

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    // Set active class to today

    $('.cfdatepicker').on('click', function () {
      setTimeout(() => {
        $('.cfdatepicker').find('td').not('.disabled').not('.new').first().addClass('today');
      }, 100)
    });

    /**
     * Hack to set Swiper.js sizes after the page has loaded
     */
    setTimeout(() => {
      window.dispatchEvent(new Event("resize"))
    }, 500)
  }
  ,
}
;
