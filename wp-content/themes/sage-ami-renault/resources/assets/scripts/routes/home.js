import Swiper from "swiper";

export default {
  init () {


    function fitVerticalSlider () {
      let categories = $('.js-desktop-categories');
      let width = categories.innerWidth();
      console.log(width);
      categories.css({
        'height': width * 0.4
      }).find('img').css({
        'width': width * 0.75
      })
    }

    fitVerticalSlider();

    $(window).on('resize', fitVerticalSlider);

    $(document).on('click', '.stock-box__radio', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $('.facetwp-checkbox[data-value="' + $(this).attr('data-value') + '"]').trigger('click');
    });

    $('.js-category').hover(function (e) {
      $('.models-hover').removeClass('active');
    });

    // JavaScript to be fired on the home page
    $(document).on('click', '.js-category', function (e) {
      e.preventDefault();

      $('.js-category').removeClass('active');
      $(this).addClass('active');

      let id = $(this).data('target');
      $('.js-model:not(#' + id + ')').addClass('model--hidden');
      $('.js-model#' + id).removeClass('model--hidden');
    });

    $('.model__close').click(function (e) {
      $('.js-category').removeClass('active');
      $('.js-model').addClass('model--hidden');
    });

    const concessionSlider = new Swiper('.concession-slider', {
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      autoplay: {
        delay: 5000
      }
    });

    /**
     *  Refresh styles on filter
     */
    $(document).on('facetwp-loaded', function () {// JavaScript to be fired on the home page, after the init JS
      $('.facetwp-checkbox.checked').each(function (i) {
        $('.stock-box__radio[data-value="' + $(this).attr('data-value') + '"]').addClass('active');
      });
    });
  },
  finalize () {

  },
};
