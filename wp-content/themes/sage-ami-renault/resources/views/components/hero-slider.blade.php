{{-- Slider hero --}}
<div class="slider">

  {{-- Background gradient --}}
  <div class="slider__gradient"></div>

  {{-- Main part of the slider component --}}

  {{-- Images in slider --}}
  <div class="slider__images">

    <div class="swiper-wrapper">
      @foreach ($slider->slides as $slide)

        @if($slide->image || $slide->video)
          <div
            class="swiper-slide slide slide--image slide--video {{ $slide->image_mobile ? 'd-none d-md-block' : '' }}">
            @if($slide->image)
              @include('partials.image', [
                  'image' => $slide->image
              ])
            @elseif($slide->video)
              <div>
              <video loop autoplay muted>
                <source src="{{ $slide->video->url }}" type="video/mp4">
              </video>
              </div>
            @endif
          </div>
        @endif

        @if($slide->image_mobile && $slide->image_mobile)
          <div
            class="swiper-slide slide slide--image d-block d-md-none">
            @include('partials.image', [
                'image' => $slide->image_mobile
            ])
          </div>
        @endif
      @endforeach
    </div>
  </div>

  {{-- Slider ocpy box --}}
  <div class="slider__copy">

    {{-- Title, text and button --}}
    <div class="swiper-wrapper">

      @foreach ($slider->slides as $slide)
        <div class="swiper-slide slide slide--copy">
          <div class="slide__title">
            {!! App::non_breaking_hyphens($slide->title) !!}
          </div>
          <div class="slide__subtitle">
            {!! App::non_breaking_hyphens($slide->subtitle) !!}
          </div>

          <a href="{{ $slide->link->url }}"
             class="button button--arrow button--big"
             target="{{ $slide->link->target }}">
            {{ $slide->link->title }}
          </a>
        </div>
      @endforeach

    </div>


    {{-- Slider navigation with arrows and bullets --}}
    @if(count($slider->slides) > 1)
      <div class="slider__navigation">

        {{-- Navigation arrows --}}
        <div class="slider__arrows">
          <div class="slider__prev"></div>
          <div class="slider__next"></div>
        </div>

        {{-- Pagination bullets --}}
        <div class="slider__bullets swiper-pagination"></div>
      </div>
    @endif
  </div>

  {{-- Optional slider menu items --}}
  @if($slider->show_slider_navigation)
    <div class="slider__menu">

      @foreach ($slider->slider_navigation as $item)
        <a href="{{ $item->link->url }}" class="slider__menu-item">

          @include('partials.image', [
            'image' => $item->hover_image,
            'class' => 'slider__menu-item__image'
          ])

          <span class="slider__menu-item__copy">
<span class="slider__menu-item__title">
  {{ $item->title }}
</span>
<span class="slider__menu-item__line">
  <span class="icon icon--plus"></span>
</span>
<span class="slider__menu-item__subtitle">
  {{ $item->link->title }}
</span>
</span>
        </a>
      @endforeach

    </div>
  @endif

</div>
