{{-- Model slide for versions and stock --}}

<div class="car-slide swiper-slide">

  {{-- Thumbnail image --}}
  <div class="model-thumbnail__image">
    @include('partials.image', [
        'image' => $car->thumbnail
    ])
  </div>

  {{-- Optional badge --}}
  @if($badge->show_badge)
    <div class="model-thumbnail__badge">
      @include('partials.badge', [
          'badge' => $badge
      ])
    </div>
  @endif


  <div class="model-thumbnail__inner">
    <h3 class="model-thumbnail__name">
      {{ $car->name }}
    </h3>

    <div class="row">
      @if(!empty($car->old_price))
        <div class="col">
          <div class="model-thumbnail__price-label">
            {{ pll__('Stock prijs') }}
          </div>

          <div class="model-thumbnail__old-price">
            {{ App::number_to_money($car->old_price) }}
          </div>
        </div>
      @endif
      @if(!empty($car->price))
        <div class="col">
          <div class="model-thumbnail__price-label">
            {{ pll__('Huidige prijs') }}
          </div>
          <div class="model-thumbnail__current-price">
            {{ App::number_to_money($car->price) }}
          </div>
        </div>
      @endif
    </div>

    @if(!empty($car->total_advantage))
      <div class="model-thumbnail__advantage">
        {{ pll__('Bespaar tot') }}
        <div class="highlight">
          {{ App::number_to_money($car->total_advantage) }}
        </div>
      </div>
    @endif

    @if(!empty($car->description))
      <div class="model-thumbnail__description">
        {!! $car->description !!}
      </div>
    @endif

    <a href="#"
       class="link link--arrow link--arrow-brand" data-toggle="modal"
       data-target="#quote-{{ get_the_ID() }}">
      {!! pll__('Krijg een offerte') !!}
    </a>

  </div>

</div>
